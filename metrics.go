package main

import (
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

const (
	statusSuccess = "success"
	statusFailure = "failure"

	actionDrop   = "drop"
	actionPass   = "pass"
	reasonTooOld = "too-old"
)

var (
	tokenLookupDuration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "idp_lookup_duration_seconds",
			Help:    "IdP token lookup and verification latency distributions. It includes cache lookup times.",
			Buckets: []float64{.005, .01, .025, .05, .1, .25, .5, 1, 2.5, 5, 10},
		},
		[]string{"method", "status"},
	)

	tokenCacheHits = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "idp_lookup_cache_total",
			Help: "IdP token lookup cache hits/misses.",
		},
		[]string{"tenant", "username", "result"},
	)

	tokenResult = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "idp_lookup_auth_result_total",
			Help: "IdP token lookup auth results.",
		},
		[]string{"tenant", "username", "result"}, // success, failure
	)

	cdnRequests = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "cdn_requests_total",
			Help: "HTTP requests on the CDN front.",
		},
		[]string{"cdn_type", "tenant", "k8s_cluster_name", "host", "status"},
	)

	cdnBytesSent = prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Name: "cdn_bytes_sent",
			Help: "The size of response in bytes, excluding HTTP headers.",
		},
		[]string{"cdn_type", "tenant", "k8s_cluster_name", "host", "status", "method", "zone"},
	)

	cdnDuration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name: "cdn_request_duration_seconds",
			Help: "The request processing time in milliseconds",
		},
		[]string{"cdn_type", "tenant", "k8s_cluster_name", "host", "method", "normalized_cache_status"},
	)

	cdnRTT = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name: "cdn_rtt_seconds",
			Help: "The TCP stack's smoothed round trip time (RTT) estimate, in microseconds.",
		},
		[]string{"cdn_type", "tenant", "k8s_cluster_name", "host", "method"},
	)

	cacheRequests = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "cdn_cache_requests_total",
			Help: "Cache requests on the CDN front.",
		},
		[]string{"cdn_type", "tenant", "k8s_cluster_name", "host", "cache_status", "normalized_cache_status", "zone"},
	)

	proxyRequestSize = prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Name: "logql_http_request_size_bytes",
			Help: "Request size going through the logql proxy.",
		},
		[]string{"tenant"},
	)

	ingressLinesIntercepted = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "logql_ingress_lines_intercepted_total",
			Help: "Number of ingress lines intercepted.",
		},
		[]string{"tenant", "action", "reason"},
	)

	interceptErrors = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "logql_intercept_errors_total",
			Help: "Number of errors while intercepting log lines.",
		},
		[]string{"tenant", "error"},
	)

	wafTraffic = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "waf_traffic",
			Help: "WAF traffic by result",
		},
		[]string{"cdn_type", "tenant", "k8s_cluster_name", "pop", "host", "result"},
	)
)

func init() {
	prometheus.MustRegister(tokenLookupDuration)
	prometheus.MustRegister(tokenCacheHits)
	prometheus.MustRegister(tokenResult)
	prometheus.MustRegister(cdnRequests)
	prometheus.MustRegister(cdnBytesSent)
	prometheus.MustRegister(cdnDuration)
	prometheus.MustRegister(cdnRTT)
	prometheus.MustRegister(cacheRequests)
	prometheus.MustRegister(proxyRequestSize)
	prometheus.MustRegister(ingressLinesIntercepted)
	prometheus.MustRegister(wafTraffic)
}

func newMetricsServer(address string) (*http.Server, error) {
	mux := http.NewServeMux()
	mux.Handle("/metrics", promhttp.Handler())

	srv := &http.Server{
		Handler:      mux,
		Addr:         address,
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
	}

	return srv, nil
}
