# Keycloak Local Operations

## Start Keycloak

Only needed for a fresh start:

```bash
cd ../../
docker run -d -p 8180:8080 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin -v $(pwd)/kcdata/:/tmp/ -e DB_VENDOR=h2 --name kc jboss/keycloak:12.0.4
```

## Export Realms

This will start an additional instance of the server inside the container. Weird, but seems to be working fine.

Do ctrl+c after the export is done.

```bash
docker exec -it keycloak /opt/jboss/keycloak/bin/standalone.sh \
-Djboss.socket.binding.port-offset=101 -Dkeycloak.migration.action=export \
-Dkeycloak.migration.provider=dir \
-Dkeycloak.migration.dir=/realm-config
```
