package main

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"regexp"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/mironet/logql-rewriter/pkg/loki"
)

var (
	timeStampLogLineRegex = regexp.MustCompile(`\[([^]]+)\]`)
)

type LokiStreamCache interface {
	Set(ctx context.Context, stream string, lastUpdated time.Time) error
	Get(ctx context.Context, stream string) (time.Time, error)
}

// NewLokiInterceptor returns a new Loki interceptor which parses incoming log
// pushes from promtail. It parses the log lines and forwards them based on a
// few conditions to "next".
//
// One of the conditions is: If the log line looks like a ingress-nginx log and
// contains a timestamp (which doesn't match the stream timestamp) it is
// forwarded to the ingress-nginx log endpoint only if the timestamp is newer
// than the last seen timestamp on this stream. This is to prevent flooding of
// loki due to a strange bug in docker and logrotation.
func NewLokiInterceptor(cache LokiStreamCache, next http.Handler) (http.Handler, error) {
	// Check if the cache is nil.
	if cache == nil {
		return nil, errors.New("cache must not be nil")
	}
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// We are only interested in POST requests.
		if r.Method != http.MethodPost {
			logrus.Debugf("intercept: not intercepting non-POST request: %s", r.Method)
			next.ServeHTTP(w, r)
			return
		}
		// Also check if the user agent is promtail.
		ua := r.UserAgent()
		if !strings.HasPrefix(ua, "promtail") { // Is that maybe too strict?
			logrus.Debugf("intercept: not intercepting non-promtail request (User-Agent was: %s): %s", ua, r.Method)
			next.ServeHTTP(w, r)
			return
		}
		tenant := r.Header.Get(headerResultingTenant)
		// Try to parse the request if we got a POST request with our loki package.
		req, err := loki.DecodeRequest(r)
		if err != nil {
			// If it's a ErrNoSnappyCompressedLokiLogLine we can just pass it through.
			var e *loki.ErrNoSnappyCompressedLokiLogLine
			if errors.As(err, &e) {
				logrus.Tracef("intercept: not intercepting non-snappy-compressed loki request: %v", err)
				next.ServeHTTP(w, r)
				return
			}
			// Handle other errors here.
			logrus.Errorf("error decoding loki request: %v", err)
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			interceptErrors.WithLabelValues(tenant, "decoding").Inc()
			return
		}
		// Remove all log lines in the request which have a timestamp older than
		// the last seen timestamp for this stream. Loop through streams and log
		// lines and check if we have a timestamp which is older than the last
		// seen timestamp for this stream. If so, remove it from the request.
		for ks, stream := range req.Streams {
			// Check each log line if they have a timestamp and look like a ingress-nginx log line.
			n := 0
			for _, entry := range stream.Entries {
				// Check if the log line looks like a ingress-nginx log line.
				t, err := isIngressLine(entry.Line)
				if err != nil {
					logrus.Tracef("intercept: not intercepting non-ingress-nginx log line: %v", err)
					stream.Entries[n] = entry
					n++
					continue
				}
				// Check if the timestamp is newer than the last seen timestamp for this stream.
				lastUpdated, err := cache.Get(r.Context(), stream.Labels)
				if err != nil {
					// If there was an error accessing redis, we should log it.
					// If the item is not found, the last seen timestamp is
					// time.Time's zero value and the error is nil.
					logrus.Errorf("intercept: not intercepting log line because of error: %v", err)
					interceptErrors.WithLabelValues(tenant, "redis-access").Inc()
					stream.Entries[n] = entry
					n++
					continue
				}

				if t.Before(lastUpdated) {
					logrus.Debugf("intercept: dropping log line from %s because it's older than the last seen timestamp: %s", r.RemoteAddr, t.String())
					logrus.Tracef("intercept: stream: %v", stream.Labels)

					ingressLinesIntercepted.WithLabelValues(tenant, actionDrop, reasonTooOld).Inc()
					// In this case we don't keep the current entry from the stream.
					continue
				} else {
					// If the timestamp is newer than or equal to the last seen
					// timestamp for this stream we update the last seen
					// timestamp for this stream.
					logrus.Tracef("intercept: cache: updating last seen timestamp for stream %s to %v", stream.Labels, t)
					if err := cache.Set(r.Context(), stream.Labels, t); err != nil {
						logrus.Errorf("error updating last seen timestamp for stream %s: %v", stream.Labels, err)
						interceptErrors.WithLabelValues(tenant, "redis-access").Inc()
					}
					ingressLinesIntercepted.WithLabelValues(tenant, actionPass, "").Inc()
					stream.Entries[n] = entry
					n++
					continue
				}
			}
			stream.Entries = stream.Entries[:n]
			// Finally we update the slice of streams in the request.
			req.Streams[ks] = stream
		}
		// We filtered the request, now we encode it again and pass it to the next handler.
		// Encode the request again.
		if err := loki.EncodeRequest(r, req); err != nil {
			logrus.Errorf("error encoding loki request: %v", err)
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			interceptErrors.WithLabelValues(tenant, "encoding").Inc()
			return
		}
		// Pass the request to the next handler.
		next.ServeHTTP(w, r)
	}), nil
}

// Here we check if the log line looks like a ingress-nginx log line. The format
// is explained here:
// https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/log-format/
func isIngressLine(line string) (time.Time, error) {
	zero := time.Time{}
	// Use regular expressions to split the log line and extract the timestamp.
	matches := timeStampLogLineRegex.FindStringSubmatch(line)
	if len(matches) < 2 {
		return zero, fmt.Errorf("could not find timestamp in log line: %s", line)
	}
	ts := matches[1]
	// Parse the timestamp.
	t, err := time.Parse("02/Jan/2006:15:04:05 -0700", ts)
	if err != nil {
		return zero, fmt.Errorf("could not parse timestamp: %s", ts)
	}
	return t, nil
}
