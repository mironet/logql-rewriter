package main

import (
	"testing"
	"time"
)

func Test_keepIngress(t *testing.T) {
	type args struct {
		line string
	}
	tests := []struct {
		name    string
		args    args
		want    time.Time
		wantErr bool
	}{
		{
			name: "Successful parsing",
			args: args{
				line: "192.168.1.1 - - [02/Jan/2006:15:04:05 -0700] \"GET / HTTP/1.1\" 200 612",
			},
			want:    time.Date(2006, time.January, 2, 15, 4, 5, 0, time.FixedZone("", -7*60*60)),
			wantErr: false,
		},
		{
			name: "Not enough fields in log line",
			args: args{
				line: "192.168.1.1 - -",
			},
			want:    time.Time{},
			wantErr: true,
		},
		{
			name: "Timestamp parsing error",
			args: args{
				line: "192.168.1.1 - - [02/Jan/2006:15:04:05]",
			},
			want:    time.Time{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := isIngressLine(tt.args.line)
			if (err != nil) != tt.wantErr {
				t.Errorf("keepIngress() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !got.Equal(tt.want) {
				t.Errorf("keepIngress() = %v, want %v", got, tt.want)
			}
		})
	}
}
