package main

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/redis/go-redis/v9"
)

type redisStreamKV struct {
	client *redis.Client
	ttl    time.Duration
}

type redisStreamKVOpt func(*redisStreamKV) error

func WithTTL(ttl time.Duration) redisStreamKVOpt {
	return func(s *redisStreamKV) error {
		s.ttl = ttl
		return nil
	}
}

func NewRedisStreamKV(ctx context.Context, addr, password string, options ...redisStreamKVOpt) (*redisStreamKV, error) {
	// Initialize Redis client
	client := redis.NewClient(&redis.Options{
		Addr:     addr,     // Redis server address
		Password: password, // Redis server password
		DB:       0,        // Replace with your Redis database index
	})

	// Check if Redis client is connected
	_, err := client.Ping(ctx).Result()
	if err != nil {
		return nil, err
	}

	// Apply options
	kvstore := &redisStreamKV{
		client: client,
		ttl:    7 * 24 * time.Hour, // Default TTL of 1 week
	}
	for _, option := range options {
		err := option(kvstore)
		if err != nil {
			return nil, err
		}
	}

	return kvstore, nil
}

func (s *redisStreamKV) Set(ctx context.Context, stream string, lastUpdated time.Time) error {
	// Convert time.Time to binary format.
	data, err := lastUpdated.MarshalBinary()
	if err != nil {
		return fmt.Errorf("error marshalling time to bytes: %w", err)
	}
	if err := s.client.Set(ctx, stream, data, s.ttl).Err(); err != nil {
		return fmt.Errorf("error setting key %s: %w", stream, err)
	}
	return nil
}

func (s *redisStreamKV) Get(ctx context.Context, stream string) (time.Time, error) {
	data, err := s.client.Get(ctx, stream).Bytes()
	if err != nil {
		if errors.Is(err, redis.Nil) {
			return time.Time{}, nil
		}
		return time.Time{}, fmt.Errorf("error getting key %s: %w", stream, err)
	}
	var lastUpdated = new(time.Time)
	if err := lastUpdated.UnmarshalBinary(data); err != nil {
		return time.Time{}, fmt.Errorf("error unmarshalling bytes to time: %w", err)
	}
	return *lastUpdated, nil
}
