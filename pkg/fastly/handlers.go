package fastly

import (
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httputil"
	"net/url"
	"path"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
	"gitlab.com/mironet/logql-rewriter/pkg/loki"
)

const (
	// Our "well-known" path prefix for handling fastly requests:
	// https://tools.ietf.org/html/rfc8615#section-3
	rfc8615prefix = "/.well-known"
	challengePath = "/fastly/logging/challenge"
	// This is the log receiving path fixed for fastly.
	logPath          = "/fastly"
	logPathSocWeblog = "/fastly/weblog"
	logPathSocWaflog = "/fastly/waflog"
)

// Handler is a middleware responding to fastly log endpoint challenges. See
// https://docs.fastly.com/en/guides/log-streaming-https for details.
type Handler struct {
	// List of service names we are configured for. Can also be a single '*'
	// entry to allow logs from any service.
	services        []string
	mux             http.Handler // All requests go through here first.
	headerName      string       // Tenant header name.
	upstreamPushAPI *url.URL     // Upstream push API to rewrite requests to.

	requestCounter         *prometheus.CounterVec
	bytesSentCounter       *prometheus.SummaryVec
	requestDurationCounter *prometheus.HistogramVec
	rttCounter             *prometheus.HistogramVec
	cacheCounter           *prometheus.CounterVec
	wafTrafficCounter      *prometheus.CounterVec
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	h.mux.ServeHTTP(w, r)
}

// Opt is an option to configure the handler.
type Opt func(*Handler) error

func NewHandler(next http.Handler, opts ...Opt) (http.Handler, error) {
	h := &Handler{}

	for _, opt := range opts {
		if err := opt(h); err != nil {
			return nil, err
		}
	}

	// Setup routing.
	mux := mux.NewRouter()
	mux.HandleFunc(path.Join(rfc8615prefix, challengePath), h.handleChallenge()).Methods(http.MethodGet)

	// Handle normal http log line.
	mux.HandleFunc(logPath, h.handleLog(next, h.handleHTTPLog)).Methods(http.MethodPost) // Chain proxy request.

	// Handle WAF weblog line.
	mux.HandleFunc(logPathSocWeblog, h.handleLog(next, h.handleWeblog)).Methods(http.MethodPost) // Chain proxy request.

	// Handle WAF waflog line.
	mux.HandleFunc(logPathSocWaflog, h.handleLog(next, h.handleWaflog)).Methods(http.MethodPost) // Chain proxy request.

	// Catch-all.
	mux.PathPrefix("/").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		next.ServeHTTP(w, r) // Chain to next handler.
	})
	h.mux = mux

	return h, nil
}

type logLineConverter func(body io.Reader, tenant string) (*loki.LokiLogLine, error)

func (h *Handler) handleHTTPLog(body io.Reader, tenant string) (*loki.LokiLogLine, error) {
	return fastly2Loki(body, tenant,
		h.requestCounter,
		h.bytesSentCounter,
		h.requestDurationCounter,
		h.rttCounter,
		h.cacheCounter,
	)
}

func (h *Handler) handleWeblog(body io.Reader, tenant string) (*loki.LokiLogLine, error) {
	return fastlyWebLog2Loki(body, tenant, h.wafTrafficCounter)
}

func (h *Handler) handleWaflog(body io.Reader, tenant string) (*loki.LokiLogLine, error) {
	return fastlyWafLog2Loki(body, tenant)
}

// The incoming fastly structure is parsed and transformed to a loki log
// line which then is forwarded upstream.
func (h *Handler) handleLog(next http.Handler, convert logLineConverter) http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		tenant := r.Header.Get(h.headerName)
		if tenant == "" {
			logrus.Error("tenant header empty")
			http.Error(rw, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}
		line, err := convert(r.Body, tenant)
		if err != nil {
			logrus.Errorf("could not parse incoming fastly log line: %v", err)
			http.Error(rw, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}
		// Attach new request body and update the Content-Length header.
		enc, err := json.Marshal(line)
		if err != nil {
			logrus.Errorf("could not encode loki log line: %v", err)
			http.Error(rw, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
		body := bytes.NewReader(enc)
		r.Body = io.NopCloser(body)
		r.ContentLength = body.Size()
		if _, ok := r.Header["Content-Length"]; ok {
			r.Header.Set("Content-Length", strconv.FormatInt(body.Size(), 10))
		}
		// Rewrite the path in the request URL to use Loki's push API.
		u := new(url.URL)
		u.Path = h.upstreamPushAPI.Path
		*r.URL = *u
		r.RequestURI = u.RequestURI()
		if logrus.IsLevelEnabled(logrus.TraceLevel) {
			dump, err := httputil.DumpRequest(r, true)
			if err != nil {
				logrus.Errorf("could not dump outgoing request: %v", err)
				http.Error(rw, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				return
			}
			logrus.Tracef("%s\n", dump)
		}
		next.ServeHTTP(rw, r)
	}
}

func (h *Handler) handleChallenge() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		for _, v := range h.services {
			encoded := v
			if v != "*" { // A '*' will not be hashed.
				hash := sha256.Sum256([]byte(v))
				encoded = hex.EncodeToString(hash[:])
			}
			// First write sends "200 OK" as status code.
			if _, err := fmt.Fprintln(rw, encoded); err != nil {
				logrus.Errorf("error while writing http response: %v", err)
				return
			}
		}
	}
}

// WithServiceIDs configures the handler to response to these service IDs.
func WithServiceIDs(services ...string) Opt {
	return func(h *Handler) error {
		h.services = services
		return nil
	}
}

// WithHeaderName sets the tenant header name.
func WithHeaderName(name string) Opt {
	return func(h *Handler) error {
		h.headerName = name
		return nil
	}
}

// WithUpstream sets the upstream Push API of the target Loki server where all
// push requests are being sent to.
func WithUpstream(upstream string) Opt {
	return func(h *Handler) error {
		up, err := url.Parse(upstream)
		if err != nil {
			return fmt.Errorf("could not parse URL for upstream API: %w", err)
		}
		h.upstreamPushAPI = up
		return nil
	}
}

// WithRequestCounter sets a function which will be called each time a single
// request has been logged.
func WithRequestCounter(c *prometheus.CounterVec) Opt {
	return func(h *Handler) error {
		h.requestCounter = c
		return nil
	}
}

func WithBytesSentCounter(c *prometheus.SummaryVec) Opt {
	return func(h *Handler) error {
		h.bytesSentCounter = c
		return nil
	}
}

func WithRequestDurationCounter(c *prometheus.HistogramVec) Opt {
	return func(h *Handler) error {
		h.requestDurationCounter = c
		return nil
	}
}

func WithRTTCounter(c *prometheus.HistogramVec) Opt {
	return func(h *Handler) error {
		h.rttCounter = c
		return nil
	}
}

func WithCacheCounter(c *prometheus.CounterVec) Opt {
	return func(h *Handler) error {
		h.cacheCounter = c
		return nil
	}
}

func WithWafTrafficCounter(c *prometheus.CounterVec) Opt {
	return func(h *Handler) error {
		h.wafTrafficCounter = c
		return nil
	}
}
