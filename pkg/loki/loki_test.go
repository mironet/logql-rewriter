package loki

import (
	"bytes"
	"io"
	"net/http"
	"os"
	"testing"
	"time"

	"github.com/gogo/protobuf/proto"
	"github.com/google/go-cmp/cmp"
	"gitlab.com/mironet/logql-rewriter/pkg/loki/logproto/push"
)

func mustReadFile(t *testing.T, name string) []byte {
	t.Helper()
	f, err := os.Open(name)
	if err != nil {
		t.Fatal(err)
	}
	defer f.Close()
	buf := new(bytes.Buffer)
	if _, err := io.Copy(buf, f); err != nil {
		t.Fatal(err)
	}
	return buf.Bytes()
}

func Test_parseRequestManually(t *testing.T) {
	file := mustReadFile(t, "testdata/01HKYYYXPQ0000000000000000")
	req := &push.PushRequest{}
	if err := parseProtoReader(bytes.NewReader(file), req, RawSnappy); err != nil {
		t.Fatal(err)
	}
	t.Logf("streams = %d", len(req.Streams))
	for _, v := range req.Streams {
		t.Logf("entries = %d", len(v.Entries))
		t.Logf("labels = %v", v.Labels)
		for k, entry := range v.Entries {
			t.Logf("entry %d: %v", k, entry.Line)
		}
	}
}

func Test_decompressRequest(t *testing.T) {
	file := mustReadFile(t, "testdata/01HKWK1BHV0000000000000000")
	type args struct {
		reader      io.Reader
		compression CompressionType
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "No compression",
			args: args{
				reader:      bytes.NewReader([]byte("test data")),
				compression: NoCompression,
			},
			wantErr: false,
		},
		{
			name: "Raw snappy compression",
			args: args{
				reader:      bytes.NewReader(file),
				compression: RawSnappy,
			},
			wantErr: false,
		},
		{
			name: "Unsupported compression type",
			args: args{
				reader:      bytes.NewReader([]byte("test data")),
				compression: CompressionType(999), // unsupported compression type
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := decompressRequest(tt.args.reader, tt.args.compression)
			if (err != nil) != tt.wantErr {
				t.Errorf("decompressRequest() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			//t.Logf("decompressRequest() = %v", string(got))
		})
	}
}

func Test_ParseProtoReader(t *testing.T) {
	file := mustReadFile(t, "testdata/01HKYYYXPQ0000000000000000")
	type args struct {
		reader      io.Reader
		req         proto.Message
		compression CompressionType
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Successful parsing",
			args: args{
				reader:      bytes.NewReader(file),
				req:         &push.PushRequest{},
				compression: RawSnappy,
			},
			wantErr: false,
		},
		{
			name: "Decompression error",
			args: args{
				reader:      bytes.NewReader([]byte("test data")),
				req:         &push.PushRequest{},
				compression: RawSnappy,
			},
			wantErr: true,
		},
		{
			name: "Unmarshalling error",
			args: args{
				reader:      bytes.NewReader(file),
				req:         &push.PushRequest{},
				compression: NoCompression,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := parseProtoReader(tt.args.reader, tt.args.req, tt.args.compression)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseProtoReader() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			t.Logf("ParseProtoReader() = %v", tt.args.req)
		})
	}
}

func TestEncodeDecodeRequest(t *testing.T) {
	// Create a dummy request
	originalReq := &push.PushRequest{
		Streams: []push.Stream{
			{
				Labels: "{job=\"testjob\"}",
				Entries: []push.Entry{
					{
						Timestamp: time.Now(),
						Line:      "Test log line",
					},
				},
			},
		},
	}

	// Encode the request
	httpReq, _ := http.NewRequest("POST", "http://localhost:3100/loki/api/v1/push", nil)
	err := EncodeRequest(httpReq, originalReq)
	if err != nil {
		t.Fatalf("Failed to encode request: %v", err)
	}

	// Decode the request
	decodedReq, err := DecodeRequest(httpReq)
	if err != nil {
		t.Fatalf("Failed to decode request: %v", err)
	}

	// Compare the original and decoded requests
	if diff := cmp.Diff(originalReq, decodedReq); diff != "" {
		t.Errorf("Original and decoded requests differ (-want +got):\n%s", diff)
	}
}
