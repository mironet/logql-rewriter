package loki

import (
	"bytes"
	"fmt"
	"io"
	"mime"
	"net/http"

	"github.com/gogo/protobuf/proto"
	"github.com/golang/snappy"
	"github.com/sirupsen/logrus"
	"gitlab.com/mironet/logql-rewriter/pkg/loki/logproto/push"
)

var (
	contentType      = http.CanonicalHeaderKey("Content-Type")
	contentEnc       = http.CanonicalHeaderKey("Content-Encoding")
	mimeTypeProtobuf = "application/x-protobuf"
)

// CompressionType for encoding and decoding requests and responses.
type CompressionType int

// Values for CompressionType
const (
	NoCompression CompressionType = iota
	RawSnappy                     // This is the default in promtail.
)

// ErrNoSnappyCompressedLokiLogLine is returned if the request body is not
// snappy compressed loki log lines. This is not an error, but a signal to the
// caller that the request body is not a snappy compressed loki log line.
type ErrNoSnappyCompressedLokiLogLine struct {
	Err error
}

func (e ErrNoSnappyCompressedLokiLogLine) Error() string {
	return fmt.Sprintf("no snappy compressed loki log line: %v", e.Err)
}

func (e ErrNoSnappyCompressedLokiLogLine) Unwrap() error {
	return e.Err
}

func errNoLog(err error) error {
	return ErrNoSnappyCompressedLokiLogLine{Err: err}
}

type LokiLabels struct {
	Tenant string `json:"tenant"`
	Host   string `json:"host"`
}

type LokiStream struct {
	Stream LokiLabels `json:"stream"`
	Values [][]string `json:"values"`
}

type LokiLogLine struct {
	Streams []LokiStream `json:"streams"`
}

// DecodeRequest parses a loki push request from an http request. It is expected
// to be snappy-compressed protobuf like what promtail sends.
func DecodeRequest(r *http.Request) (*push.PushRequest, error) {
	var body io.Reader
	contentEncoding := r.Header.Get(contentEnc)
	switch contentEncoding {
	case "snappy", "":
		// Note: HTTP clients do not need to set this header, but they sometimes
		// do. Promtail doesn't seem to set it.
		body = r.Body
	default:
		return nil, errNoLog(fmt.Errorf("unsupported content encoding %q", contentEncoding))
	}

	contentType := r.Header.Get(contentType)
	contentType, _, err := mime.ParseMediaType(contentType)
	if err != nil {
		return nil, fmt.Errorf("could not parse content type: %w", err)
	}
	if contentType != mimeTypeProtobuf && contentType != "" {
		return nil, errNoLog(fmt.Errorf("unsupported content type %q", contentType))
	}
	// We (probably) have a protobuf message.
	var req push.PushRequest
	if err := parseProtoReader(body, &req, RawSnappy); err != nil {
		return nil, fmt.Errorf("could not parse push request: %w", err)
	}
	return &req, nil
}

// EncodeRequest encodes a loki push request and replaces the request body in
// the http request passed. The old body is probably alread read and closed, but
// if not, it will be after this call.
func EncodeRequest(r *http.Request, req *push.PushRequest) error {
	buf, err := proto.Marshal(req)
	if err != nil {
		return fmt.Errorf("could not marshal push request: %w", err)
	}
	// Save the old body for draining and closing to free resources.
	oldBody := r.Body
	if oldBody != nil {
		defer func() {
			// Fully read the body to avoid leaking resources.
			if _, err := io.Copy(io.Discard, oldBody); err != nil {
				logrus.Debugf("could not read request body: %v", err)
			}
			if err := oldBody.Close(); err != nil {
				logrus.Debugf("could not close request body: %v", err)
			}
		}()
	}
	// Now we have a protobuf message, compress it.
	buf = snappy.Encode(nil, buf)
	r.Body = io.NopCloser(bytes.NewReader(buf))
	r.ContentLength = int64(len(buf))
	r.Header.Set(contentType, mimeTypeProtobuf)
	r.Header.Set(contentEnc, "snappy") // Do we need this?
	return nil
}

func parseProtoReader(reader io.Reader, req proto.Message, compression CompressionType) error {
	body, err := decompressRequest(reader, compression)
	if err != nil {
		return fmt.Errorf("could not decompress request: %w", err)
	}
	req.Reset()
	if u, ok := req.(proto.Unmarshaler); ok { // Do we need this?
		err = u.Unmarshal(body)
	} else {
		err = proto.NewBuffer(body).Unmarshal(req)
	}
	if err != nil {
		return fmt.Errorf("could not unmarshal request: %w", err)
	}

	return nil
}

func decompressRequest(reader io.Reader, compression CompressionType) ([]byte, error) {
	switch compression {
	case NoCompression:
		return io.ReadAll(reader)
	case RawSnappy:
		var err error
		var buf bytes.Buffer
		if _, err = io.Copy(&buf, reader); err != nil {
			return nil, fmt.Errorf("could not read snappy compressed body: %w", err)
		}
		body, err := snappy.Decode(nil, buf.Bytes())
		if err != nil {
			return nil, fmt.Errorf("snappy: could not decompress bytes: %w", err)
		}
		return body, nil
	default:
		return nil, fmt.Errorf("unsupported compression type %d", compression)
	}
}
