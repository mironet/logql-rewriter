package cache

import (
	"bytes"
	"context"
	"encoding/gob"
	"errors"
	"fmt"
	"time"

	"github.com/redis/go-redis/v9"
	"github.com/sirupsen/logrus"
)

type Redis struct {
	client     *redis.Client
	tick       *time.Ticker // Never stops.
	lockoutWin time.Duration
	lockoutMax int
}

var (
	failCounterLockoutWindow = 24 * time.Hour // Time window for failed attempts before lockout.
	failCounterMax           = 3              // Max number of failed attempts during window before lockout.
)

type RedisOpt func(*Redis) error

func WithFailCounterLockoutWindow(win time.Duration, max int) RedisOpt {
	return func(r *Redis) error {
		if win <= 0 {
			win = failCounterLockoutWindow
		}
		r.lockoutWin = win
		if max < 0 {
			max = failCounterMax
		}
		r.lockoutMax = max
		if r.lockoutMax == 0 {
			return nil
		}
		r.tick = time.NewTicker(r.lockoutWin / time.Duration(r.lockoutMax))

		go func() {
			δ := (r.lockoutWin / time.Duration(r.lockoutMax))
			if δ <= 0 {
				logrus.Warnf("redis: fail counter lockout window too small: %v", r.lockoutWin)
				δ = time.Minute
			}
			for t := range r.tick.C {
				// Decrease the fail counters by one.
				func() {
					ctx, cancel := context.WithTimeout(context.Background(), δ)
					defer cancel()
					if err := r.decrementFailCounters(ctx); err != nil {
						logrus.Errorf("redis: error decrementing fail counters: %v", err)
						return
					}
					logrus.Debugf("redis: fail counters decremented at %v", t)
				}()
			}
		}()

		return nil
	}
}

func NewRedis(ctx context.Context, addr, password string, opts ...RedisOpt) (*Redis, error) {
	// Initialize Redis client.
	client := redis.NewClient(&redis.Options{
		Addr:     addr,     // Redis server address
		Password: password, // Redis server password
		DB:       0,        // Replace with your Redis database index
	})

	// Check if Redis client is connected.
	_, err := client.Ping(ctx).Result()
	if err != nil {
		return nil, err
	}

	r := &Redis{
		client: client,
	}

	// Apply options.
	for _, opt := range opts {
		if err := opt(r); err != nil {
			return nil, err
		}
	}

	return r, nil
}

func (r *Redis) Put(ctx context.Context, key string, cred Credential) error {
	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	if err := enc.Encode(cred); err != nil {
		return fmt.Errorf("redis: error encoding credential: %w", err)
	}
	if err := r.client.Set(ctx, key, buf.Bytes(), cred.ExpiresAt.Sub(time.Now())).Err(); err != nil {
		return fmt.Errorf("redis: error setting key %s: %w", key, err)
	}
	return nil
}

const (
	failCounterPrefix = "c_fail"
)

// Periodically decrement fail counters by one to allow for new attempts.
func (r *Redis) decrementFailCounters(ctx context.Context) error {
	keys, err := r.client.Keys(ctx, fmt.Sprintf("%s:*", failCounterPrefix)).Result()
	if err != nil {
		return fmt.Errorf("redis: error getting keys: %w", err)
	}
	for _, key := range keys {
		// Start a transaction.
		txf := func(tx *redis.Tx) error {
			// Get current value
			val, err := tx.Get(ctx, key).Int()
			if err != nil && !errors.Is(err, redis.Nil) {
				return fmt.Errorf("redis: error getting key %s: %w", key, err)
			}

			if val <= 1 {
				// Remove key if value would become 0 or less.
				if err := tx.Del(ctx, key).Err(); err != nil {
					return fmt.Errorf("redis: error deleting key %s: %w", key, err)
				}
				logrus.Debugf("redis: removed fail counter for key %s", key)
				return nil
			}

			// Decrement if value would still be positive.
			if err := tx.Decr(ctx, key).Err(); err != nil {
				return fmt.Errorf("redis: error decrementing key %s: %w", key, err)
			}
			logrus.Debugf("redis: decremented fail counter for key %s", key)

			return nil
		}

		// Execute the transaction.
		if err := r.client.Watch(ctx, txf, key); err != nil {
			return fmt.Errorf("redis: transaction failed for key %s: %w", key, err)
		}
	}
	return nil
}

// Fail increases the fail counter for the given IP address. If the counter
// doesn't exist yet, it is created in redis.
func (r *Redis) Fail(ctx context.Context, remoteIP string) error {
	if r.lockoutMax == 0 {
		return nil // No-op.
	}
	key := fmt.Sprintf("%s:%s", failCounterPrefix, remoteIP)
	if err := r.client.Incr(ctx, key).Err(); err != nil {
		return fmt.Errorf("redis: error incrementing fail counter for %s: %w", remoteIP, err)
	}
	return nil
}

// IsBlocked checks if the given IP address is blocked due to too many failed
// requests.
func (r *Redis) IsBlocked(ctx context.Context, remoteIP string) (bool, error) {
	if r.lockoutMax == 0 {
		return false, nil
	}
	key := fmt.Sprintf("%s:%s", failCounterPrefix, remoteIP)
	val, err := r.client.Get(ctx, key).Int()
	if err != nil {
		if errors.Is(err, redis.Nil) {
			return false, nil
		}
		return false, fmt.Errorf("redis: error getting fail counter for %s: %w", remoteIP, err)
	}
	return val >= r.lockoutMax, nil
}

func (r *Redis) Get(ctx context.Context, key string) (Credential, bool) {
	data, err := r.client.Get(ctx, key).Bytes()
	if err != nil {
		if errors.Is(err, redis.Nil) {
			return Credential{}, false
		}
		logrus.Warnf("redis: error getting key %s: %v", key, err)
		return Credential{}, false
	}
	dec := gob.NewDecoder(bytes.NewReader(data))
	var cred Credential
	if err := dec.Decode(&cred); err != nil {
		logrus.Warnf("redis: error decoding credential: %v", err)
		return Credential{}, false
	}
	return cred, true
}

func (r *Redis) Del(ctx context.Context, key string) error {
	if err := r.client.Del(ctx, key).Err(); err != nil {
		if errors.Is(err, redis.Nil) {
			return nil
		}
		return fmt.Errorf("redis: error deleting key %s: %w", key, err)
	}
	return nil
}
