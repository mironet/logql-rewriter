package cache

import (
	"bytes"
	"context"
	"encoding/gob"
	"fmt"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
)

func Test_requestCache_Put(t *testing.T) {
	ctx := context.Background()
	type wantFunc func(c *Request, key string) error
	type args struct {
		key  string
		cred Credential
	}
	tests := []struct {
		name string
		args args
		want wantFunc
	}{
		{
			name: "0 time left",
			args: args{
				key:  "test",
				cred: Credential{},
			},
			want: func(c *Request, key string) error {
				time.Sleep(time.Millisecond)
				cred, ok := c.Get(ctx, key)
				if !ok {
					return nil
				}
				return fmt.Errorf("got t = %v, expected none", cred)
			},
		},
		{
			name: "enough time left",
			args: args{
				key:  "test",
				cred: Credential{ExpiresAt: time.Now().AddDate(1, 0, 0)},
			},
			want: func(c *Request, key string) error {
				time.Sleep(time.Millisecond)
				_, ok := c.Get(ctx, key)
				if !ok {
					return fmt.Errorf("got no value, expected one")
				}
				return nil
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := New()
			_ = c.Put(ctx, tt.args.key, tt.args.cred)
			if err := tt.want(c, tt.args.key); err != nil {
				t.Error(err)
			}
		})
	}
}

func Test_gobEncode(t *testing.T) {
	now := time.Now()
	cred := Credential{
		Hash:      []byte("test"),
		Token:     []byte("test"),
		ExpiresAt: now,
		Tenant:    "test",
	}
	var buf bytes.Buffer
	encoder := gob.NewEncoder(&buf)
	if err := encoder.Encode(cred); err != nil {
		t.Fatal(err)
	}
	var cred2 Credential
	decoder := gob.NewDecoder(&buf)
	if err := decoder.Decode(&cred2); err != nil {
		t.Fatal(err)
	}
	if diff := cmp.Diff(cred, cred2); diff != "" {
		t.Errorf("gobEncode() mismatch (-want +got):\n%s", diff)
	}
	fmt.Println(cred, cred2)
}
