package cache

import (
	"context"
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/redis/go-redis/v9"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var (
	defaultTestRedisAddr = "localhost:6379"
)

func TestRedis(t *testing.T) {
	redisAddr := os.Getenv("REDIS_ADDR")
	if redisAddr == "" {
		// Check if we can acess the default Redis address
		_, err := NewRedis(context.Background(), defaultTestRedisAddr, "")
		if err != nil {
			t.Logf("REDIS_ADDR not set and default Redis address not accessible: %v", err)
			t.Skip("REDIS_ADDR not set, skipping Redis tests")
			return
		}
		redisAddr = defaultTestRedisAddr
	}

	ctx := context.Background()
	r, err := NewRedis(ctx, redisAddr, "")
	assert.NoError(t, err)

	cred := Credential{
		Hash:      []byte("testhash"),
		Token:     []byte("testtoken"),
		ExpiresAt: time.Now().Add(time.Millisecond * 10),
		Tenant:    "testtenant",
	}

	key := "testkey"

	// Test Put
	err = r.Put(ctx, key, cred)
	assert.NoError(t, err)

	// Test Get
	gotCred, ok := r.Get(ctx, key)
	assert.True(t, ok)
	assert.Equal(t, cred.Hash, gotCred.Hash)
	assert.Equal(t, cred.Token, gotCred.Token)
	assert.WithinDuration(t, cred.ExpiresAt, gotCred.ExpiresAt, 0)
	assert.Equal(t, cred.Tenant, gotCred.Tenant)

	// Test Get after expiration
	time.Sleep(time.Millisecond * 20) // Assuming the credential expires in 10 ms.
	_, ok = r.Get(ctx, key)
	assert.False(t, ok)
}

func TestRedisFail(t *testing.T) {
	redisAddr := os.Getenv("REDIS_ADDR")
	if redisAddr == "" {
		_, err := NewRedis(context.Background(), defaultTestRedisAddr, "")
		if err != nil {
			t.Logf("REDIS_ADDR not set and default Redis address not accessible: %v", err)
			t.Skip("REDIS_ADDR not set, skipping Redis tests")
			return
		}
		redisAddr = defaultTestRedisAddr
	}

	ctx := context.Background()
	r, err := NewRedis(ctx, redisAddr, "", WithFailCounterLockoutWindow(failCounterLockoutWindow, failCounterMax))
	assert.NoError(t, err)

	t.Run("increment fail counter for new IP", func(t *testing.T) {
		testIP := "192.168.1.1"

		// Ensure clean state
		key := fmt.Sprintf("%s:%s", failCounterPrefix, testIP)
		r.client.Del(ctx, key)

		// Initially should be 0
		val, err := r.client.Get(ctx, key).Int()
		if err != redis.Nil {
			assert.NoError(t, err)
		}
		assert.Equal(t, 0, val)

		// Increment once
		err = r.Fail(ctx, testIP)
		assert.NoError(t, err)

		// Get actual value from Redis
		val, err = r.client.Get(ctx, key).Int()
		assert.NoError(t, err)
		assert.Equal(t, 1, val)
	})

	t.Run("multiple increments for same IP", func(t *testing.T) {
		testIP := "192.168.1.1"

		// Ensure clean state
		key := fmt.Sprintf("%s:%s", failCounterPrefix, testIP)
		r.client.Del(ctx, key)

		// Initially should be 0
		val, err := r.client.Get(ctx, key).Int()
		if err != redis.Nil {
			assert.NoError(t, err)
		}
		assert.Equal(t, 0, val)

		// Increment once
		err = r.Fail(ctx, testIP)
		assert.NoError(t, err)

		// Get actual value from Redis
		val, err = r.client.Get(ctx, key).Int()
		assert.NoError(t, err)
		assert.Equal(t, 1, val)
	})

	t.Run("fail count persists", func(t *testing.T) {
		testIP := "192.168.1.4"

		// Ensure clean state
		key := fmt.Sprintf("%s:%s", failCounterPrefix, testIP)
		r.client.Del(ctx, key)

		// Set up initial state
		err := r.Fail(ctx, testIP)
		assert.NoError(t, err)

		// Verify in new Redis instance
		r2, err := NewRedis(ctx, redisAddr, "")
		assert.NoError(t, err)

		// Get actual value from Redis using second instance
		val, err := r2.client.Get(ctx, key).Int()
		assert.NoError(t, err)
		assert.Equal(t, 1, val)
	})
}

func TestRedisDecrementFailCounters(t *testing.T) {
	redisAddr := os.Getenv("REDIS_ADDR")
	if redisAddr == "" {
		_, err := NewRedis(context.Background(), defaultTestRedisAddr, "")
		if err != nil {
			t.Skip("REDIS_ADDR not set, skipping Redis tests")
			return
		}
		redisAddr = defaultTestRedisAddr
	}

	ctx := context.Background()
	r, err := NewRedis(ctx, redisAddr, "")
	assert.NoError(t, err)

	t.Run("decrement single counter", func(t *testing.T) {
		testIP := "192.168.1.10"
		key := fmt.Sprintf("%s:%s", failCounterPrefix, testIP)

		// Setup: Set counter to 2
		err := r.client.Set(ctx, key, 2, 0).Err()
		assert.NoError(t, err)

		// Test decrement
		err = r.decrementFailCounters(ctx)
		assert.NoError(t, err)

		// Verify counter was decremented
		val, err := r.client.Get(ctx, key).Int()
		assert.NoError(t, err)
		assert.Equal(t, 1, val)

		// Cleanup
		r.client.Del(ctx, key)
	})

	t.Run("decrement multiple counters", func(t *testing.T) {
		ips := []string{"192.168.1.11", "192.168.1.12", "192.168.1.13"}
		keys := make([]string, len(ips))

		// Setup: Set multiple counters
		for i, ip := range ips {
			keys[i] = fmt.Sprintf("%s:%s", failCounterPrefix, ip)
			err := r.client.Set(ctx, keys[i], 3, 0).Err()
			assert.NoError(t, err)
		}

		// Test decrement
		err = r.decrementFailCounters(ctx)
		assert.NoError(t, err)

		// Verify all counters were decremented
		for _, key := range keys {
			val, err := r.client.Get(ctx, key).Int()
			assert.NoError(t, err)
			assert.Equal(t, 2, val)
		}

		// Cleanup
		for _, key := range keys {
			r.client.Del(ctx, key)
		}
	})

	t.Run("handle empty key set", func(t *testing.T) {
		// Clean all fail counter keys
		keys, _ := r.client.Keys(ctx, fmt.Sprintf("%s:*", failCounterPrefix)).Result()
		for _, key := range keys {
			r.client.Del(ctx, key)
		}

		// Test decrement with no keys
		err := r.decrementFailCounters(ctx)
		assert.NoError(t, err)
	})

	t.Run("decrement to zero removes key", func(t *testing.T) {
		testIP := "192.168.1.14"
		key := fmt.Sprintf("%s:%s", failCounterPrefix, testIP)

		// Setup: Set counter to 1
		err := r.client.Set(ctx, key, 1, 0).Err()
		assert.NoError(t, err)

		// Test decrement
		err = r.decrementFailCounters(ctx)
		assert.NoError(t, err)

		// Verify key is gone
		exists, err := r.client.Exists(ctx, key).Result()
		assert.NoError(t, err)
		assert.Equal(t, int64(0), exists)
	})
}

func TestRedis_IsBlocked(t *testing.T) {
	redisAddr := os.Getenv("REDIS_ADDR")
	if redisAddr == "" {
		_, err := NewRedis(context.Background(), defaultTestRedisAddr, "")
		if err != nil {
			t.Logf("REDIS_ADDR not set and default Redis address not accessible: %v", err)
			t.Skip("Skipping Redis tests")
			return
		}
		redisAddr = defaultTestRedisAddr
	}

	ctx := context.Background()
	r, err := NewRedis(ctx, redisAddr, "", WithFailCounterLockoutWindow(time.Hour, 3))
	require.NoError(t, err)

	t.Run("when lockout is disabled", func(t *testing.T) {
		r2, err := NewRedis(ctx, redisAddr, "", WithFailCounterLockoutWindow(0, 0))
		require.NoError(t, err)

		blocked, err := r2.IsBlocked(ctx, "192.168.1.1")
		assert.NoError(t, err)
		assert.False(t, blocked)
	})

	t.Run("when no previous failures", func(t *testing.T) {
		blocked, err := r.IsBlocked(ctx, "192.168.1.2")
		assert.NoError(t, err)
		assert.False(t, blocked)
	})

	t.Run("when under threshold", func(t *testing.T) {
		ip := "192.168.1.3"
		key := fmt.Sprintf("%s:%s", failCounterPrefix, ip)

		// Set counter to 2 (under threshold of 3)
		err := r.client.Set(ctx, key, 2, 0).Err()
		require.NoError(t, err)

		blocked, err := r.IsBlocked(ctx, ip)
		assert.NoError(t, err)
		assert.False(t, blocked)
	})

	t.Run("when at threshold", func(t *testing.T) {
		ip := "192.168.1.4"
		key := fmt.Sprintf("%s:%s", failCounterPrefix, ip)

		// Set counter to 3 (at threshold)
		err := r.client.Set(ctx, key, 3, 0).Err()
		require.NoError(t, err)

		blocked, err := r.IsBlocked(ctx, ip)
		assert.NoError(t, err)
		assert.True(t, blocked)
	})

	t.Run("when over threshold", func(t *testing.T) {
		ip := "192.168.1.5"
		key := fmt.Sprintf("%s:%s", failCounterPrefix, ip)

		// Set counter to 4 (over threshold)
		err := r.client.Set(ctx, key, 4, 0).Err()
		require.NoError(t, err)

		blocked, err := r.IsBlocked(ctx, ip)
		assert.NoError(t, err)
		assert.True(t, blocked)
	})

	t.Run("when redis returns error", func(t *testing.T) {
		// Create a Redis client with invalid connection
		r2, err := NewRedis(ctx, "invalid:6379", "")
		if err == nil {
			blocked, err := r2.IsBlocked(ctx, "192.168.1.6")
			assert.Error(t, err)
			assert.False(t, blocked)
		}
	})
}
