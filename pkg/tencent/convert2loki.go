package tencent

import (
	"encoding/json"
	"fmt"
	"io"
	"sort"
	"strconv"
	"time"

	"gitlab.com/mironet/logql-rewriter/pkg/loki"
)

type TencentLogLine struct {
	Time         json.Number `json:"time"`
	ClientIP     string      `json:"client_ip"`
	Host         string      `json:"host"`
	URL          string      `json:"url"`
	Param        string      `json:"param"`
	Proto        string      `json:"proto"`
	Method       string      `json:"method"`
	HTTPCode     json.Number `json:"http_code"`
	Referer      string      `json:"referer"`
	UA           string      `json:"ua"`
	FileSize     json.Number `json:"file_size"`
	RspSize      json.Number `json:"rsp_size"`
	Hit          string      `json:"hit"`
	ISP          string      `json:"isp"`
	RemotePort   json.Number `json:"remote_port"`
	UUID         string      `json:"uuid"`
	Version      json.Number `json:"version"` // CDN real-time log version
	RequestRange string      `json:"request_range"`
	RequestTime  json.Number `json:"request_time"`
	AppID        json.Number `json:"app_id"`
	Prov         string      `json:"prov"`
}

var (
	insideTest bool
	testNow    = time.Date(2006, 02, 01, 03, 04, 05, 0, time.UTC)
)

func tencent2Loki(in io.Reader, tenant string) (*loki.LokiLogLine, error) {
	var tencentLog []TencentLogLine
	if err := json.NewDecoder(in).Decode(&tencentLog); err != nil {
		return nil, fmt.Errorf("could not parse Tencent log: %w", err)
	}

	// We use this map to group by host.
	streams := make(map[string][][]string)

	// Keep the same timestamp for all log entries we received in one single
	// request. This is not a stream so we should be fine.
	//
	// Docs stating: > logs sent to Loki for every stream must be in
	// timestamp-ascending order; logs with identical timestamps are only
	// allowed if their content differs.
	//
	// https://grafana.com/docs/loki/latest/api/#post-lokiapiv1push
	now := time.Now().UnixNano()
	if insideTest {
		now = testNow.UnixNano()
	}

	for _, v := range tencentLog {
		enc, err := json.Marshal(v)
		if err != nil {
			return nil, fmt.Errorf("could not marshal Tencent log line to json: %w", err)
		}

		entry := []string{strconv.FormatInt(now, 10), string(enc)}
		streams[v.Host] = append(streams[v.Host], entry)
	}

	lokiLog := new(loki.LokiLogLine)
	// Make sure output is deterministic.
	keys := make([]string, 0)
	for k := range streams {
		keys = append(keys, k)
	}

	sort.Strings(keys)

	for _, k := range keys {
		stream := loki.LokiStream{
			Stream: loki.LokiLabels{
				Tenant: tenant,
				Host:   k,
			},
			Values: streams[k],
		}
		lokiLog.Streams = append(lokiLog.Streams, stream)
	}

	return lokiLog, nil
}
