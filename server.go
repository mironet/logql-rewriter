package main

import (
	"net/http"
	"time"
)

func newServer(address string, routes http.Handler) (*http.Server, error) {
	mux := http.NewServeMux()
	mux.Handle("/", routes)

	mux.Handle("/healthz", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// 200 Ok.
	}))

	srv := &http.Server{
		Handler:      mux,
		Addr:         address,
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
	}

	return srv, nil
}
